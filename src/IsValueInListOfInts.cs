using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace EcoLogic.AspNetCoreValidations
{
    public abstract class IsValueInListOfInts : ValidationAttribute
    {
        protected abstract IEnumerable<int> ValidValues { get; }
        public const string Message = "{0} value is not valid.";

        public override bool IsValid(object value)
        {
            return value == null || value is int deliveredValue && ValidValues.Contains(deliveredValue);
        }
    }
}
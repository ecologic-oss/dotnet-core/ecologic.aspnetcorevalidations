using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace EcoLogic.AspNetCoreValidations
{
    public abstract class IsValueInListOfStrings : ValidationAttribute
    {
        protected abstract IEnumerable<string> ValidValues { get; }
        public const string Message = "{0} value is not valid.";

        public override bool IsValid(object value)
        {
            return value == null || value is string deliveredValue && ValidValues.Contains(deliveredValue);
        }
    }
}
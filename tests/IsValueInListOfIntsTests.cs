using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace EcoLogic.AspNetCoreValidations.Tests
{
    public class IsValueInListOfIntsTests
    {
        [Theory]
        [InlineData(new[] {123, 456}, 123, true)]
        [InlineData(new[] {123, 456}, 999, false)]
        [InlineData(new[] {123, 456}, null, true)]
        [InlineData(new int[] { }, 999, false)]
        public void Tests_IsValueInListOfStrings(IEnumerable<int> validValues, object givenValue, bool expectedResult)
        {
            // Arrange
            var attribute = new MyIntListValidationAttribute(validValues);

            // Act
            var result = attribute.IsValid(givenValue);

            // Assert
            Assert.Equal(expectedResult, result);
        }

        private class MyIntListValidationAttribute : IsValueInListOfInts
        {
            public MyIntListValidationAttribute(IEnumerable<int> validValues)
            {
                ValidValues = validValues.ToList();
            }

            protected sealed override IEnumerable<int> ValidValues { get; }
        }
    }
}
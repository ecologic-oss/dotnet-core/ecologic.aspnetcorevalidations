using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace EcoLogic.AspNetCoreValidations.Tests
{
    public class Tests
    {
        [Theory]
        [InlineData(new[] {"abc", "xyz"}, "abc", true)]
        [InlineData(new[] {"abc", "xyz"}, "qwerty", false)]
        [InlineData(new[] {"abc", "xyz"}, null, true)]
        [InlineData(new string[] { }, "qwerty", false)]
        public void Tests_IsValueInListOfStrings(IEnumerable<string> validValues, object givenValue, bool expectedResult)
        {
            // Arrange
            var attribute = new MyStringListValidationAttribute(validValues);

            // Act
            var result = attribute.IsValid(givenValue);

            // Assert
            Assert.Equal(expectedResult, result);
        }

        private class MyStringListValidationAttribute : IsValueInListOfStrings
        {
            public MyStringListValidationAttribute(IEnumerable<string> validValues)
            {
                ValidValues = validValues.ToList();
            }

            protected sealed override IEnumerable<string> ValidValues { get; }
        }
    }
}
# EcoLogic.AspNetCoreValidations - A set of validations for asp .net core models

**EcoLogic.AspNetCoreValidations** provides a set of validations which can be used for asp .net core models.

## IsValueInListOfStrings

Checks if a given string is within a list of strings.

**Usage:**

- **Create a new class with the valid strings**

```csharp
using System.Collections.Generic;
using EcoLogic.AspNetCoreValidations;

public class IsValidStatus : IsValueInListOfStrings
{
    protected override IEnumerable<string> ValidValues => new List<string> {"ok", "canceled", "delayed"};
}
```

- **Use the class inside your Model**

```csharp
public class MitgliedBaseDto
{
    [IsValidStatus(ErrorMessage = IsValueInListOfStrings.Message)] // Or use a custom error Message here
    public string Status { get; set; }
}
```

## IsValueInListOfInts

Checks if a given int is within a list of ints.

**Usage:**

- **Create a new class with the valid ints**

```csharp
using System.Collections.Generic;
using EcoLogic.AspNetCoreValidations;

public class IsValidStatus : IsValueInListOfInts
{
    protected override IEnumerable<int> ValidValues => new List<int> {4, 14};
}
```

- **Use the class inside your Model**

```csharp
public class MitgliedBaseDto
{
    [IsValidStatus(ErrorMessage = IsValueInListOfInts.Message)] // Or use a custom error Message here
    public int StatusId { get; set; }
}
```
